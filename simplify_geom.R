######################
# SCRIPT PERMETTANT DE SIMPLIFIER LES GEOMETRIES ET AGGREGER LES DONNEES DANS LA MAILLE
######################


# Libraries
library(sf)
library(rmapshaper)

# Import données enrichies
hop <- st_read(dsn = "data/geom.gpkg", layer = "hop", quiet = TRUE)
com_insee <- st_read(dsn = "data/geom.gpkg", layer = "com_insee", quiet = TRUE)
com_dist <- st_read(dsn = "data/geom.gpkg", layer = "com_dist", quiet = TRUE)
com_dist <- st_set_geometry(com_dist, NULL)
country <- st_read(dsn = "data/geom.gpkg", layer = "country", quiet = TRUE)

com_insee <- com_insee[com_insee$INSEE_REG != "94",]

# Export countries
country <- st_transform(country, 4326)
st_write(country, "data-out/country.geojson")

# Export données d'hôpitaux (geojson)
hop <- st_transform(hop, 4326)
hop00 <- hop[hop$year == "2000",]
hop18 <- hop[hop$year == "2018",]

st_write(hop00, "data-out/hop00.geojson")
st_write(hop00, "data-out/hop18.geojson")


# Simplify geometries and keep attributes
com_cent <- st_centroid(com_insee)

fr <- st_union(com_insee)
fr <- ms_simplify(fr, keep = .05)

v <- st_voronoi(st_union(com_cent), fr)
v <- st_as_sf(st_intersection(st_cast(v), fr))
v$ID <- row.names(v)

inter <- st_intersection(com_cent, v)
insee <- aggregate(x = list(INSEE_COM = inter$INSEE_COM), 
                      by = list(ID = inter$ID), 
                      FUN = head,1)


v <- merge(v, insee, by = "ID", all.x = TRUE)
v$ID <- NULL

com_cent <- st_set_geometry(com_cent, NULL)
head(com_cent)
com_cent <- com_cent[,c("INSEE_COM", "POP_TOT_99", "POP_TOT_16",  "NOM_COM", "EPCI", "ZE2010", "UU2010",
                        "BV2012", "AU2010", "INSEE_REG")]

v <- merge(v, com_dist[,-2], by = "INSEE_COM", all.x = TRUE)
v <- merge(v, com_cent, by = "INSEE_COM", all.x = TRUE)

## Aggregate regions
reg <- aggregate(x = v[,"INSEE_REG"], 
                   by = list(REG = v$INSEE_REG), 
                   FUN = head,1)

reg <- st_transform(reg, 4326)
st_write(reg, "data-out/reg.geojson")

# Aggregate ZEMP
library(readxl)
lib <- data.frame(read_xlsx("data/ZE2010_au_01-01-2020.xlsx", sheet = "ZE2010", skip = 5))

ind <- colnames(com_dist)
ind <- ind[3:length(ind)]
zemp <- aggregate(v[,c("ZE2010", ind)],
                  by = list(ZE2010 = v$ZE2010),
                  FUN = median)

zemp2 <- aggregate(v[,c("POP_TOT_99", "POP_TOT_16")],
                  by = list(ZE2010 = v$ZE2010),
                  FUN = sum)

zemp <- merge(zemp, st_drop_geometry(zemp2), by = "ZE2010", all.x = TRUE)
zemp <- merge(zemp, lib[,c("ZE2010", "LIBZE2010")], by = "ZE2010", all.x = TRUE)

zemp <- st_transform(zemp, 4326)
st_write(zemp, "data-out/zemp.geojson")

# Aires Urbaines
library(readxl)
lib <- data.frame(read_xls("data/AU2010_au_01-01-2018_V2.xls", sheet = "Composition_communale", skip = 5))

lib$CATA <- lib$CATAEU2010
lib$CATAEU2010[lib$CATAEU2010 == "111"] <- "Grand pôle"
lib$CATAEU2010[lib$CATAEU2010 == "112"] <- "Couronne grand pôle"
lib$CATAEU2010[lib$CATAEU2010 == "120"] <- "Commune multipolarisée"
lib$CATAEU2010[lib$CATAEU2010 == "211"] <- "Moyen pôle"
lib$CATAEU2010[lib$CATAEU2010 == "212"] <- "Couronne moyen pôle"
lib$CATAEU2010[lib$CATAEU2010 == "221"] <- "Petit pôle"
lib$CATAEU2010[lib$CATAEU2010 == "222"] <- "Petit pôle"
lib$CATAEU2010[lib$CATAEU2010 == "300"] <- "Commune multipolarisée"
lib$CATAEU2010[lib$CATAEU2010 == "400"] <- "Commune isolée"

lib$AU2010 <- paste0(lib$AU2010, lib$CATA)

au <- merge(v, lib[,c("CODGEO", "AU2010", "CATAEU2010", "LIBAU2010")], by.x = "INSEE_COM",
            by.y = "CODGEO", all.x = TRUE)

au2 <- aggregate(au[,c("AU2010.y", "CATAEU2010", "LIBAU2010")],
                by = list(AU2010 = au$AU2010.y),
                FUN = head,1)

au3 <- aggregate(au[,c("AU2010.y", ind)],
                  by = list(AU2010 = au$AU2010.y),
                  FUN = median)

au4 <- aggregate(au[,c("POP_TOT_99", "POP_TOT_16")],
                   by = list(AU2010 = au$AU2010.y),
                   FUN = sum)

au <- merge(au2, st_drop_geometry(au3), by = "AU2010", all.x = TRUE)
au <- merge(au, st_drop_geometry(au4), by = "AU2010", all.x = TRUE)

au$AU2010.y.x <- NULL
au$AU2010.y.y <- NULL

au <- st_transform(au, 4326)
st_write(au, "data-out/au.geojson")


# Communes
v <- merge(v, lib[,c("CODGEO", "AU2010", "CATAEU2010", "LIBAU2010")], by.x = "INSEE_COM",
            by.y = "CODGEO", all.x = TRUE)

ind <- colnames(v)[2:15]
v <- v[,c(ind, "NOM_COM",  "CATAEU2010", "LIBAU2010")]
v <- v[order(v$LIBAU2010), ]

v <- st_transform(v, 4326)
st_write(v, "data-out/com_voronoi.geojson")


# Simplify communes
com <- ms_simplify(com_insee, keep = .05)
com <- com[,c(1:2)]
com <- merge(com, lib[,c("CODGEO", "LIBAU2010", "CATAEU2010")],
             by.x = "INSEE_COM", by.y = "CODGEO", all.x = TRUE)

com <- merge(com, com_dist[,c(1, 2:length(com_dist))], by = "INSEE_COM",
             all.x = TRUE)

colnames(com)[2] <- "NOM_COM"
com$NOM_COM.y <- NULL
com <- st_transform(com, 4326)
st_write(com, "data-out/com.geojson")

# Départements
dep <- com
dep$dep <- substr(com$INSEE_COM, 1,2)
dep <- aggregate(x = dep[,"dep"], 
                 by = list(DEP = dep$dep), 
                 FUN = head,1)
head(dep)
head(com_dist)
